
#include <limits>
#include <map>
#include <vector>
#include <queue>
#include <iostream>

const int VX = 9;
const unsigned int INF = std::numeric_limits<unsigned int>::max();
typedef std::pair<int, unsigned int> pair;

enum {
  A = 0,
  B = 1,
  E = 2,
  J = 3,
  L = 4,
  K = 5,
  N = 6,
  O = 7,
  T = 8
};

struct greater_pair {
  bool operator () (const pair &p1, const pair &p2) {
    return p1.second > p2.second;
  }
};

int main() {
  std::map<char, int> ids { { 'A', A }, { 'B', B }, { 'E', E }, { 'J', J }, { 'L', L }, { 'K', K },{ 'N', N }, { 'O', O }, { 'T', T } };
  std::vector<std::vector<pair>> net {
    { { B, 193 }, { J, 377 }, { T, 102 } }, // A
    { { A, 193 }, { L, 179 }, { O, 120 } }, // B
    { { O, 240 }                         }, // E
    { { A, 377 }, { O, 106 }             }, // J
    { { B, 179 }, { K, 216 }, { N, 180 } }, // L
    { { L, 216 }                         }, // K
    { { L, 180 }                         }, // N
    { { B, 120 }, { E, 240 }, { J, 106 } }, // O
    { { A, 102 }                         }  // T
  };

  int a, b;
  char ac, bc;
  std::cin >> ac >> bc;
  a = ids[ac];
  b = ids[bc];

  std::vector<unsigned int> d(VX, INF);
  std::vector<bool> p(VX, false);
  std::priority_queue<pair, std::vector<pair>, greater_pair> q;
  d[a] = 0;
  q.push(pair(a, d[a]));
  while (!q.empty()) {
    int f = q.top().first;
    q.pop();
    if (p[f])
      continue;
    p[f] = true;
    for (auto t : net[f]) {
      if (d[t.first] > d[f] + t.second)
        d[t.first] = d[f] + t.second;
      q.push(pair(t.first, d[t.first]));
    }
  }

  std::cout << d[b];

  return 0;
}